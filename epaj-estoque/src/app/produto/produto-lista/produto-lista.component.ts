import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-produto-lista',
  templateUrl: './produto-lista.component.html',
  styleUrls: ['./produto-lista.component.css']
})
export class ProdutoListaComponent implements OnInit {

  listaCopmpleta : any;

  listaNomesprodutos : string[] = ['Bombril', 'Detergente'];

  constructor() { 
    
    this.listaCopmpleta = [
                            {'id':1,
                             'nome':'Bombril',
                             'quantidade':7},
                            {'id':2,
                             'nome':"Detergente",
                             'quantidade':2},
                           ]
  }

  ngOnInit() {
  }

}
